<?php
    
$PathToConfigXML = '/var/www/html/modules/fussball_tabelle/assets/Config.xml';

echo '<h2 class="module__title">' . _("fussball_tabelle_title") . '</h2>';

function Show1Drittel($HowManyTeams, $ShowPositionNumber, $ShowLogo, $ShowGoalDifference, $ShowPoints, $TextAlign, $fixtures)
{
    //1. Drittel der Tabelle
    echo "<table id='tbl1Drittel' cellspacing='0' cellpadding='0' style='float:left;'>";
    echo "<tbody>";
    
    for ($i = 0; $i < ceil($HowManyTeams/3); $i++)
    {
        echo "<tr>";
        
        if ($ShowPositionNumber == "true") {
            echo "<td style=\"text-align:right;\">"; echo $fixtures['standings']['0']['table'][$i]['position']; echo "."; echo "</td>";
        }
        
        if ($ShowLogo == "true") {
            echo '<td style="text-align:center;"><img height="22px" src="'; echo $fixtures['standings']['0']['table'][$i]['team']['crestUrl']; echo '"> </td>';    //Bildhöhe bei Bedarf anpassen. Aktuell: font-size: 20, Höhe: 22px 
        }
                
        echo "<td style=\"text-align:$TextAlign;\">&nbsp"; echo $fixtures['standings']['0']['table'][$i]['team']['name']; echo "</td>";
        
        if ($ShowGoalDifference == "true") {
            echo "<td style=\"text-align:right; padding-left:10px;\">"; echo $fixtures['standings']['0']['table'][$i]['goalDifference']; echo "</td>";
        }
        
        if ($ShowPoints == "true") {
            echo "<td style=\"text-align:right; padding-left:10px;\">"; echo $fixtures['standings']['0']['table'][$i]['points']; echo "</td>";
        }
        
        echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
}

function Show2Drittel($HowManyTeams, $ShowPositionNumber, $ShowLogo, $ShowGoalDifference, $ShowPoints, $TextAlign, $fixtures)
{
    //2. Drittel der Tabelle
    echo "<table id='tbl2Drittel' cellspacing='0' cellpadding='0' style='float:left;'>";
    echo "<tbody>";
    
    for ($i = ceil($HowManyTeams/3); $i < (($HowManyTeams/3)+($HowManyTeams/3)); $i++)
    {
        echo "<tr>";
        
        if ($ShowPositionNumber == "true") {
            echo "<td style=\"text-align:right;\">"; echo $fixtures['standings']['0']['table'][$i]['position']; echo "."; echo "</td>";
        }
        
        if ($ShowLogo == "true") {
            echo '<td style="text-align:center;"><img height="22px" src="'; echo $fixtures['standings']['0']['table'][$i]['team']['crestUrl']; echo '"> </td>';    //Bildhöhe bei Bedarf anpassen. Aktuell: font-size: 20, Höhe: 22px 
        }
                
        echo "<td style=\"text-align:$TextAlign;\">&nbsp"; echo $fixtures['standings']['0']['table'][$i]['team']['name']; echo "</td>";
        
        if ($ShowGoalDifference == "true") {
            echo "<td style=\"text-align:right; padding-left:10px;\">"; echo $fixtures['standings']['0']['table'][$i]['goalDifference']; echo "</td>";
        }
        
        if ($ShowPoints == "true") {
            echo "<td style=\"text-align:right; padding-left:10px;\">"; echo $fixtures['standings']['0']['table'][$i]['points']; echo "</td>";
        }
        
        echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
}

function Show3Drittel($HowManyTeams, $ShowPositionNumber, $ShowLogo, $ShowGoalDifference, $ShowPoints, $TextAlign, $fixtures)
{
    //3. Drittel der Tabelle
    echo "<table id='tbl3Drittel' cellspacing='0' cellpadding='0' style='float:left;'>";
    echo "<tbody>";
    
    for ($i = ceil(($HowManyTeams/3)+($HowManyTeams/3)); $i < $HowManyTeams; $i++)
    {
        echo "<tr>";
        
        if ($ShowPositionNumber == "true") {
            echo "<td style=\"text-align:right;\">"; echo $fixtures['standings']['0']['table'][$i]['position']; echo "."; echo "</td>";
        }
        
        if ($ShowLogo == "true") {
            echo '<td style="text-align:center;"><img height="22px" src="'; echo $fixtures['standings']['0']['table'][$i]['team']['crestUrl']; echo '"> </td>';    //Bildhöhe bei Bedarf anpassen. Aktuell: font-size: 20, Höhe: 22px 
        }
                
        echo "<td style=\"text-align:$TextAlign;\">&nbsp"; echo $fixtures['standings']['0']['table'][$i]['team']['name']; echo "</td>";
        
        if ($ShowGoalDifference == "true") {
            echo "<td style=\"text-align:right; padding-left:10px;\">"; echo $fixtures['standings']['0']['table'][$i]['goalDifference']; echo "</td>";
        }
        
        if ($ShowPoints == "true") {
            echo "<td style=\"text-align:right; padding-left:10px;\">"; echo $fixtures['standings']['0']['table'][$i]['points']; echo "</td>";
        }
        
        echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
}
    
if (file_exists($PathToConfigXML)) {
    $xml = simplexml_load_file($PathToConfigXML);
        
    $LeagueID = $xml->League;
    $ShowGoalDifference =$xml->ShowGoalDifference;
    $ShowPositionNumber = $xml->ShowPositionNumber;
    $ShowPoints = $xml->ShowPoints;
    $ShowLogo = $xml->ShowLogo;
    $TextAlign = $xml->TextAlign;
    $APIKey = $xml->APIKey;
    $Halfwidth = $xml->HalfWidth;
}
else //Standardwerte falls Datei nicht existiert
{
    $LeagueID = 2002;
    $ShowGoalDifference = "true";
    $ShowPositionNumber = "true";
    $ShowPoints = "true";
    $ShowLogo = "false";
    $TextAlign = "left";
    $Halfwidth = 1;
    $APIKey = "???";
}

    $uri = 'http://api.football-data.org/v2/competitions/'."$LeagueID".'/standings';
    $reqPrefs['http']['method'] = 'GET';
    $reqPrefs['http']['header'] = 'X-Auth-Token: '."$APIKey";
    $stream_context = stream_context_create($reqPrefs);
    $response = file_get_contents($uri, false, $stream_context);
    $fixtures = json_decode($response, true);
    
    $HowManyTeams = count($fixtures['standings']['0']['table']);
    
    
    //DIVs mit Tabellen-Dritteln füllen
    echo '<div id="1.Drittel" style="display: none;">'; Show1Drittel($HowManyTeams, $ShowPositionNumber, $ShowLogo, $ShowGoalDifference, $ShowPoints, $TextAlign, $fixtures); echo '</div>';
    echo '<div id="2.Drittel" style="display: none;">'; Show2Drittel($HowManyTeams, $ShowPositionNumber, $ShowLogo, $ShowGoalDifference, $ShowPoints, $TextAlign, $fixtures); echo '</div>';
    echo '<div id="3.Drittel" style="display: none;">'; Show3Drittel($HowManyTeams, $ShowPositionNumber, $ShowLogo, $ShowGoalDifference, $ShowPoints, $TextAlign, $fixtures); echo '</div>';
    
    //Bei voller Breite alle Drittel anzeigen und paddingLeft hinzufügen
    echo '<script>var ClassOfParentNode = document.getElementById("1.Drittel").parentNode.className;
		if (ClassOfParentNode.includes("fullwidth"))
			{document.getElementById("1.Drittel").style.display = "block";
			document.getElementById("2.Drittel").style.display = "block";
			document.getElementById("tbl2Drittel").style.paddingLeft = "60px";
			document.getElementById("3.Drittel").style.display = "block";
			document.getElementById("tbl3Drittel").style.paddingLeft = "60px";}
		</script>';
    
    //Bei halber Breite ausgewähltes Drittel anzeigen
if ($Halfwidth==1) {
    echo '<script>var ClassOfParentNode = document.getElementById("1.Drittel").parentNode.className;
		if (ClassOfParentNode.includes("halfwidth"))
			{document.getElementById("1.Drittel").style.display = "block";}</script>';
}
elseif ($Halfwidth==2) {
    echo '<script>var ClassOfParentNode = document.getElementById("1.Drittel").parentNode.className;
		if (ClassOfParentNode.includes("halfwidth"))
			{document.getElementById("2.Drittel").style.display = "block";}</script>';
}
elseif ($Halfwidth==3) {
    echo '<script>var ClassOfParentNode = document.getElementById("1.Drittel").parentNode.className;
		if (ClassOfParentNode.includes("halfwidth"))
			{document.getElementById("3.Drittel").style.display = "block";}</script>';
}
    
    echo '<script language="javascript" type="text/javascript">setTimeout(function(){window.location.reload(1);}, 3600000) /*Reload alle 60 Minuten. Wert nicht verändern, ansonsten erfolgt eine Sperrung aufgrund zu vielen Aufrufen der API*/;</script>';
?>
