<?php
$PathToConfigXML = '/var/www/html/modules/fussball_tabelle/assets/Config.xml';
include ('/../../../config/glancrConfig.php');

	if(isset($_POST['SubmitButton'])) //check if form was submitted
	{ 
		$doc = new DOMDocument( );

		$xmlRoot = $doc->createElement("xml");
		$xmlRoot = $doc->appendChild($xmlRoot);
		
		$ele = $doc->createElement('League');
		$ele->nodeValue = $_POST['ListLeague'];
		$xmlRoot->appendChild( $ele );
		
		$ele = $doc->createElement('ShowGoalDifference');
		$ele->nodeValue = $_POST['ListShowGoalDifference'];
		$xmlRoot->appendChild( $ele );
		
		$ele = $doc->createElement('ShowPositionNumber');
		$ele->nodeValue = $_POST['ListShowPositionNumber'];
		$xmlRoot->appendChild( $ele );
		
		$ele = $doc->createElement('ShowPoints');
		$ele->nodeValue = $_POST['ListShowPoints'];
		$xmlRoot->appendChild( $ele );
		
		$ele = $doc->createElement('ShowLogo');
		$ele->nodeValue = $_POST['ListShowLogo'];
		$xmlRoot->appendChild( $ele );
		
		$ele = $doc->createElement('TextAlign');
		$ele->nodeValue = $_POST['ListTextAlign'];
		$xmlRoot->appendChild( $ele );
		
		$ele = $doc->createElement('APIKey');
		$ele->nodeValue = $_POST['ListAPIKey'];
		$xmlRoot->appendChild( $ele );
		
		$ele = $doc->createElement('HalfWidth');
		$ele->nodeValue = $_POST['ListHalfWidth'];
		$xmlRoot->appendChild( $ele );
		
		$doc->save($PathToConfigXML);		
		
		setConfigValue("reload", "1");
	}
	
    if (file_exists($PathToConfigXML)) 
    {
        $xml = simplexml_load_file($PathToConfigXML);
    }
    else //Falls Datei nicht vorhanden sein, dann erstellen
    {
        $doc = new DOMDocument( );
        
        $xmlRoot = $doc->createElement("xml");
        $xmlRoot = $doc->appendChild($xmlRoot);
        
        $ele = $doc->createElement('League');
        $ele->nodeValue = 2002;
        $xmlRoot->appendChild( $ele );
        
        $ele = $doc->createElement('ShowGoalDifference');
        $ele->nodeValue = "true";
        $xmlRoot->appendChild( $ele );
        
        $ele = $doc->createElement('ShowPositionNumber');
        $ele->nodeValue = "false";
        $xmlRoot->appendChild( $ele );
        
        $ele = $doc->createElement('ShowPoints');
        $ele->nodeValue = "true";
        $xmlRoot->appendChild( $ele );
		
		$ele = $doc->createElement('ShowLogo');
        $ele->nodeValue = "false";
        $xmlRoot->appendChild( $ele );
        
        $ele = $doc->createElement('TextAlign');
        $ele->nodeValue = "left";
        $xmlRoot->appendChild( $ele );
		
		$ele = $doc->createElement('APIKey');
        $ele->nodeValue = "";
        $xmlRoot->appendChild( $ele );
		
		$ele = $doc->createElement('HalfWidth');
        $ele->nodeValue = "1";
        $xmlRoot->appendChild( $ele );
        
        $doc->save($PathToConfigXML);
        
        sleep(2); //Warten bis Datei geschrieben ist.
        
        $xml = simplexml_load_file($PathToConfigXML);
    }
    
	_("fussball_tabelle_title");
	_("fussball_tabelle_description");
   
    echo _("tabelle_WelcheLiga"); #echo 'Für welche Liga soll die Tabelle angezeigt werden?';
    echo nl2br ("\n");
    echo '<form action="" method="post">';
    echo '<select name="ListLeague">';
    echo "<option value='2013'"; if($xml->League == 2013)echo ' selected="selected"'; echo ">Série A (Brazil)</option>
	<option value='2015'"; if($xml->League == 2015)echo ' selected="selected"'; echo ">Ligue 1</option>
	<option value='2021'"; if($xml->League == 2021)echo ' selected="selected"'; echo ">Premier League</option>
	<option value='2002'"; if($xml->League == 2002)echo ' selected="selected"'; echo ">1. Bundesliga</option>
	<option value='2019'"; if($xml->League == 2019)echo ' selected="selected"'; echo ">Serie A (Italy)</option>
	<option value='2003'"; if($xml->League == 2003)echo ' selected="selected"'; echo ">Eredivisie</option>
	<option value='2017'"; if($xml->League == 2017)echo ' selected="selected"'; echo ">Primeira Liga</option>
	<option value='2014'"; if($xml->League == 2014)echo ' selected="selected"'; echo ">Primera Division</option>";
    echo '</select>';
	
    echo nl2br ("\n");
    echo nl2br ("\n");
    echo _("tabelle_Tabellenplatznummer"); #echo 'Soll die Nummer des Tabellenplatzes angezeigt werden?';
    echo nl2br ("\n");
    echo '<select name="ListShowPositionNumber">';
    echo '<option value=true'; if($xml->ShowPositionNumber == "true") echo ' selected="selected"'; echo '>'; echo _("anzeigen"); echo'</option>';
    echo '<option value=false'; if($xml->ShowPositionNumber == "false") echo ' selected="selected"'; echo '>'; echo _("nichtanzeigen"); echo'</option>';
    echo '</select>';
	
	echo nl2br ("\n");
    echo nl2br ("\n");
    echo _("tabelle_Logos"); #echo 'Sollen die Logos der Mannschaften angezeigt werden? (Sofern diese hinterlegt sind)';
    echo nl2br ("\n");
    echo '<select name="ListShowLogo">';
    echo '<option value=true'; if($xml->ShowLogo == "true") echo ' selected="selected"'; echo '>'; echo _("anzeigen"); echo'</option>';
    echo '<option value=false'; if($xml->ShowLogo == "false") echo ' selected="selected"'; echo '>'; echo _("nichtanzeigen"); echo'</option>';
    echo '</select>';
	
	echo nl2br ("\n");
    echo nl2br ("\n");
    echo _("tabelle_Torverhältnis"); #echo 'Soll das Torverhältnis angezeigt werden?';
    echo nl2br ("\n");
    echo '<select name="ListShowGoalDifference" >';
    echo '<option value=true'; if($xml->ShowGoalDifference == "true") echo ' selected="selected"'; echo '>'; echo _("anzeigen"); echo'</option>';
    echo '<option value=false'; if($xml->ShowGoalDifference == "false") echo ' selected="selected"'; echo '>'; echo _("nichtanzeigen"); echo'</option>';
    echo '</select>';
    
    echo nl2br ("\n");
    echo nl2br ("\n");
    echo _("tabelle_Punkte");#echo 'Sollen die Punkte der Mannschaften angezeigt werden?';
    echo nl2br ("\n");
    echo '<select name="ListShowPoints">';
    echo '<option value=true'; if($xml->ShowPoints == "true") echo ' selected="selected"'; echo '>'; echo _("anzeigen"); echo'</option>';
    echo '<option value=false'; if($xml->ShowPoints == "false") echo ' selected="selected"'; echo '>'; echo _("nichtanzeigen"); echo'</option>';
    echo '</select>';
    
    echo nl2br ("\n");
    echo nl2br ("\n");
    echo _("tabelle_Textausrichtung");#echo 'Welche Textausrichtung soll die Tabelle haben?';
    echo nl2br ("\n");
    echo '<select name="ListTextAlign">';
    echo '<option value=left'; if($xml->TextAlign == "left") echo ' selected="selected"'; echo '>'; echo _("linksbündig"); echo '</option>';
    echo '<option value=right'; if($xml->TextAlign == "right") echo ' selected="selected"'; echo '>'; echo _("rechtsbündig"); echo '</option>';
    echo '<option value=center'; if($xml->TextAlign == "center") echo ' selected="selected"'; echo '>'; echo _("zentriert"); echo '</option>';
    echo '</select>';
	
	echo nl2br ("\n");
    echo nl2br ("\n");
    echo _("tabelle_APIKey");#echo 'API-Key hier eingeben';
    echo nl2br ("\n");
	$APIKey = $xml->APIKey;
    echo '<input type="text" name="ListAPIKey" value="'; echo $APIKey; echo '"></input>';
	
	echo nl2br ("\n");
    echo nl2br ("\n");
    echo _("tabelle_HalbeBreite");#echo 'Falls halbe Modulbreite: Welches Drittel der Tabelle soll angezeigt werden?';
    echo nl2br ("\n");
    echo '<select name="ListHalfWidth">';
    echo '<option value=1'; if($xml->HalfWidth == "1") echo ' selected="selected"'; echo '>'; echo _("1. Drittel"); echo '</option>';
    echo '<option value=2'; if($xml->HalfWidth == "2") echo ' selected="selected"'; echo '>'; echo _("2. Drittel"); echo '</option>';
    echo '<option value=3'; if($xml->HalfWidth == "3") echo ' selected="selected"'; echo '>'; echo _("3. Drittel"); echo '</option>';
    echo '</select>';
    
    echo nl2br ("\n");
    echo nl2br ("\n");
	echo _("tabelle_Änderungenhinweis"); #echo 'Das Modul aktualisiert sich alle 60 Minuten automatisch.';
	echo nl2br ("\n");
	echo '<div class="block__add" id="withings__edit">';
	echo '<button class="withings__edit--button" type="submit" name="SubmitButton"><span>'; echo _("Speichern"); echo '</span></button>';
	echo '</div>';
	
    //echo '<input type="submit" value="Okay" name="SubmitButton"/>';
	echo nl2br ("\n");
    echo nl2br ("\n");
    echo '</form>'
?>