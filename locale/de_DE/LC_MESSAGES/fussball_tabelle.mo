��          �      \      �  
   �  
   �  
   �  	   �     �          "     9     F     T     b     q     �     �     �     �     �     �  	     �    
   �  
   �  
   �  	          6        N     _     l     {  �   �  K     4   i  5   �  .   �  )     3   -  8   a  	   �                                 	                                             
                     1. Drittel 2. Drittel 3. Drittel Speichern anzeigen fussball_tabelle_description fussball_tabelle_title linksbündig nichtanzeigen rechtsbündig tabelle_APIKey tabelle_HalbeBreite tabelle_Punkte tabelle_Tabellenplatznummer tabelle_Textausrichtung tabelle_Torverhältnis tabelle_WelcheLiga tabelle_Änderungenhinweis zentriert Project-Id-Version: Tabelle Backend (GER)
POT-Creation-Date: 2018-08-08 20:40+0200
PO-Revision-Date: 2018-08-08 20:40+0200
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: ../../../backend
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _
X-Poedit-SearchPath-0: template.php
 1. Drittel 2. Drittel 3. Drittel Speichern Anzeigen Zeigt die Tabelle einer ausgewählten Fußballliga an. Fußball-Tabelle Linksbündig Nicht anzeigen Rechtsbündig Hier bitte den persönlichen API-Schlüssel eingeben. Dieser kann unter https://www.football-data.org/client/register kostenlos angefordert werden. Falls halbe Modulbreite: Welches Drittel der Tabelle soll angezeigt werden? Sollen die Punkte der Mannschaften angezeigt werden? Soll die Nummer des Tabellenplatzes angezeigt werden? Welche Textausrichtung soll die Tabelle haben? Soll das Torverhältnis angezeigt werden? Für welche Liga soll die Tabelle angezeigt werden? Das Modul aktualisiert sich alle 60 Minuten automatisch. Zentriert 