��          �      \      �  
   �  
   �  
   �  	   �     �          "     9     F     T     b     q     �     �     �     �     �     �  	     �    	   �  	     	               *   !     L  
   `     k     w  y   �  C   �  ,   A  $   n  *   �  #   �  /   �  2        E                                 	                                             
                     1. Drittel 2. Drittel 3. Drittel Speichern anzeigen fussball_tabelle_description fussball_tabelle_title linksbündig nichtanzeigen rechtsbündig tabelle_APIKey tabelle_HalbeBreite tabelle_Punkte tabelle_Tabellenplatznummer tabelle_Textausrichtung tabelle_Torverhältnis tabelle_WelcheLiga tabelle_Änderungenhinweis zentriert Project-Id-Version: Tabelle Backend
POT-Creation-Date: 2018-08-08 20:39+0200
PO-Revision-Date: 2018-08-09 08:35+0200
Last-Translator: Tobias Grasse <tg@glancr.de>
Language-Team: 
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: ../../../backend
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _
X-Poedit-SearchPath-0: template.php
 1st third 2nd third 3rd third Save Show Shows the table of a chosen soccer league. Soccer League Table Align left Do not show Align right Please enter your personal API key here. This can be requested for free at https://www.football-data.org/client/register. If half module width: Which third of the table should be displayed? Should the points of the teams be displayed? Should the rank number be displayed? What text alignment should the table have? Should the Goal Ratio be displayed? For which league should the table be displayed? The module updates automatically every 60 minutes. Centered 